#!/bin/bash

echo "Removing old cat/dat files....."
rm -rf ext_01.*

echo "Building new cat/dat files....."
wine ~/SteamApp/common/X\ Tools/XRCatTool.exe -in ./ -out ./ext_01.cat -exclude "^.git*" -exclude ".dae" -exclude "README.md" -exclude "content.xml" -exclude "tools/"

echo "Done"